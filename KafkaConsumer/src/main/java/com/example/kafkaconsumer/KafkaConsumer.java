package com.example.kafkaconsumer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);


    @KafkaListener(
            topics = "my-five-kafka-topic", groupId = "mts",
            containerFactory = "kafkaListenerContainerFactory")
    public void consumeMessage(Message message) {

        logger.info("message = " + message);
    }


}
