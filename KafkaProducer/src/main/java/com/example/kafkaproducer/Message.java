package com.example.kafkaproducer;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@ToString
public class Message {

    private String id;
    private String content;

}
