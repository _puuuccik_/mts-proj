package com.example.kafkaproducer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
public class KafkaProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaProducer.class);

    @Value(value = "${topic.name}")
    private String topicName;

    @Autowired
    private KafkaTemplate<String, Message> kafkaTemplate;

    public void send(Message payload) {
        kafkaTemplate.send(topicName, payload);
        LOGGER.info("sending payload='{}' to topic='{}'", payload, topicName);
    }
}
