package com.example.kafkaproducer;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/kafka")
public class MessageController {

    @Autowired
    private KafkaProducer producer;

    @PostMapping("/send")
    public ResponseEntity<?> sendMessage(@RequestBody Message message) {

        producer.send(message);
        return ResponseEntity.ok("Message was sent");
    }
}
